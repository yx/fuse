CC := tcc

OUT := fuse

SRC := main.c

$(OUT): $(SRC)
	$(CC) $(SRC) -o $(OUT)

.PHONY: clean

clean:
	rm -f $(OUT)
