# Fuse
## An esoteric programming language based on wires

# Basics
## Sparks
Sparks are the runners of the program. They can each hold one numeric value,
and can execute 'simultaneously' (each spark takes one step at a time).
Sparks travel along wires, and execute instructions
## Program Start
`=` - Terminal - Sparks will be spawned onto every connected wire to the
terminal, effectively acting as the program start. Sparks follow the direction
of their starting wire.
## Wires
`|` - Vertical Wire - Sparks can travel vertically through this
`-` - Horizontal Wire - Sparks can travel horizontally through this
Note that sparks can split when they encounter a junction. For example,
`=-|` will allow a horizontally travelling spark to split into two identical
sparks travelling along the direction of the vertical wire. This split only
occurs when a spark follows its current direction into a branch.
`+` - Junction - Sparks will continue travelling in their current direction
without splitting. Accepts input from all sides, effectively a NOP.
If a spark travels onto an empty space, it is removed from the program.
## Instructions
Instructions act like junctions in all cases, except that they will have a
side effect on the program state or the sparks.
`~` - Fuse - A spark may travel over this wire from any direction, and it is then 
removed
`v` - Push - A spark pushes its value to the data stack.
`^` - Pop -  A spark pops the top of stack and stores the value.
`:` - Duplicate - Duplicates the top value of the stack.
`%` - Swap - Swaps the top two stack values.
`)` - Rotate - Moves the bottom of the stack to the top.
`1`...`9` - Digit - A spark multiplies its current value by 10 and adds
digit to its value (easy way to construct decimal numbers in program).
`z` - Zero - Set value of spark to 0 (not necesarry, but useful)
`i` - Increment - A spark increases its value by 1.
`d` - Decrement - A spark decreases its value by 1.
`a` - Add - Spark pops top of stack and adds it to its value.
`s` - Subtract - Spark pops top of stack and subtracts it from its value.
`n` - Number - Spark takes user input as a decimal number and stores it.
`c` - Character - Spark takes user input as an ASCII character and stores it.
`.` - Output (Number) - Outputs spark's value as a decimal number.
`,` - Output (Character) - Outputs spark's value as a character.
`#` - Gate - IFF a spark's value is 0, it continues forwards. Otherwise, it
changes its direction to the left.
This set of rules should be enough to do most basic computing tasks, and is
probably Turing-complete.
