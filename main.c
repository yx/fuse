#include <stdio.h>
#include <stdlib.h>

#define STACK_DEPTH 16

enum Direction {
	Up = 0,
	Left = 1,
	Down = 2,
	Right = 3
}; // allows for addition to directions

struct Spark {
	int value;
	enum Direction dir;
	int x, y;
	struct Spark *prev; // sparks will be allocated in a reverse linked list
};

struct Stack {
	unsigned char sp;
	int data[STACK_DEPTH];
};

struct Field {
	struct Spark *last;
	char **code; // code will be read into a buffer
	int *lengths; // keep track of line lengths
	int rows; // keep track of how many rows to free at the end
	struct Stack stack;
};

static struct Field field;

void
stackPush(int value)
{
	if (field.stack.sp >= STACK_DEPTH) {
		printf("stack overflow\n");
		exit(-1);
	}
	field.stack.data[field.stack.sp] = value;
	field.stack.sp++;
}

int
stackPop()
{
	if (field.stack.sp == 0) {
		printf("stack underflow\n");
		exit(-1);
	}
	field.stack.sp--;
	return field.stack.data[field.stack.sp];
}

// creates spark and adds it to list
void
newSpark(int value, enum Direction dir, int x, int y)
{
	struct Spark *new = malloc(sizeof(struct Spark));
	if (!new) {
		printf("could not allocate new spark\n");
		exit(-1);
	}
	new->value = value;
	new->dir = dir;
	new->prev = field.last;
	new->x = x;
	new->y = y;
	field.last = new;
}

// removes spark from list, assumes it exists
void
killSpark(struct Spark *spark)
{
	// check the edge case if the last spark is to be removed
	if (spark == field.last) {
		field.last = spark->prev;
		free(spark);
		return;
	}
	// first, find the spark to be removed
	struct Spark *current = field.last;
	struct Spark *previous;
	while (current != spark) {
		previous = current;
		current = current->prev;
	}
	
	previous->prev = current->prev; // remove current from list
	free(spark); // free the spark
}

void
cleanup()
{
	if (field.last) { // check needed because I dereference field.first
		struct Spark *previous;
		struct Spark *current = field.last;
		while (current->prev) {
			previous = current;
			current = current->prev;
			free(previous);
		}
	}
	for (int i = 0; i < field.rows; i++) {
		free(field.code[i]);
	}
	free(field.lengths);
	free(field.code);
}

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("usage: %s [program]\n", argv[0]);
		exit(-1);
	}
	atexit(cleanup);
	field.stack.sp = 0; // initialize stack
	field.last = NULL;
	field.rows = 16; // default to 16 lines of code
	field.code = malloc(field.rows * sizeof(char *));
	field.lengths = malloc(field.rows * sizeof(int)); // track out of bounds
	if (!field.code) {
		printf("code field allocation error\n");
		exit(-1);
	}

	FILE *code = fopen(argv[1], "r");
	if (!code) {
		printf("could not open %s\n", argv[1]);
		exit(-1);
	}

	// load the code file into 2D array
	int currentRow = 0; // leave a blank row
	char *line = NULL;
	size_t linesize = 0;
	ssize_t length;
	while ((length = getline(&line, &linesize, code)) > 0) {
		if (currentRow == field.rows) {
			field.rows *= 2;
			field.code = realloc(field.code, field.rows * sizeof(char *));
			field.lengths = realloc(field.lengths, field.rows * sizeof(int));
			if (!field.code) {
				printf("code field allocation error\n");
				exit(-1);
			}
		}
		field.code[currentRow] = malloc((length - 1) * sizeof(char));
		field.lengths[currentRow] = 0;
		for (int i = 0; line[i] != '\n' && line[i] != EOF && i < length; i++) {
			field.code[currentRow][i] = line[i];
			field.lengths[currentRow]++; // this way guarantees correct line sizes
		}
		currentRow++;
	}
	// because the unused rows aren't allocated we can avoid freeing them
	field.rows = currentRow;
	free(line);
	fclose(code);

	// now we can start our program
	// locate all starting locations
	for (int row = 0; row < field.rows; row++) {
		for (int col = 0; field.code[row][col] > 0; col++) {
			if (field.code[row][col] == '=') { // found starting location
				// spawn sparks facing every direction
				// if they don't need to exist, they'll die iteration 1
				newSpark(0, Up, col, row);
				newSpark(0, Left, col, row);
				newSpark(0, Down, col, row);
				newSpark(0, Right, col, row);
			}
		}
	}

	while (field.last) { // program ends when sparks die
		struct Spark *current = field.last;
		while (current) { // update each spark
			switch (current->dir) {
			case Up:
				current->y--;
				break;
			case Left:
				current->x--;
				break;
			case Down:
				current->y++;
				break;
			case Right:
				current->x++;
				break;
			}
			// kill out of bounds sparks
			if (current->y < 0 || current->y >= field.rows ||
				current->x < 0 || current->x >= field.lengths[current->y]) {
				struct Spark *skip = current->prev;
				killSpark(current);
				current = skip;
				continue;
			}
			char instruction = field.code[current->y][current->x];
			int temp, temp2;
			struct Spark *skip;
			switch (instruction) {
			// wires
			case '|':
				if (current->dir == Left || current->dir == Right) {
					current->dir = (current->dir + 1) % 4; // turn left
					newSpark(current->value, (current->dir + 2) % 4, current->x, current->y);
				}
				break;
			case '-':
				if (current->dir == Up || current->dir == Down) {
					current->dir = (current->dir + 1) % 4; // turn left
					newSpark(current->value, (current->dir + 2) % 4, current->x, current->y);
				}
				break;
			case '+':
				break;
			case '~':
				field.code[current->y][current->x] = ' ';
				break;
			// instructions
			case 'v':
				stackPush(current->value);
				break;
			case '^':
				current->value = stackPop();
				break;
			case ':':
				temp = stackPop();
				stackPush(temp);
				stackPush(temp);
				break;
			case '%':
				temp = stackPop();
				temp2 = stackPop();
				stackPush(temp);
				stackPush(temp2);
				break;
			case ')':
				stackPush(field.stack.data[0]);
				// we have an extra copy of bottom
				for (int i = 0; i < field.stack.sp - 1; i++) {
					field.stack.data[i] = field.stack.data[i + 1];
				}
				field.stack.sp--; // return to regular stack size
				// TEST THIS!!!!!
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				current->value *= 10;
				current->value += (instruction - '0');
				break;
			case 'z':
				current->value = 0;
				break;
			case 'i':
				current->value++;
				break;
			case 'd':
				current->value--;
				break;
			case 'a':
				current->value += stackPop();
				break;
			case 's':
				current->value -= stackPop();
				break;
			case 'n':
				if (scanf("%i", &current->value) == 0) {
					printf("error reading input\n");
					exit(-1);
				}
				break;
			case 'c':
				if (scanf("%c", &current->value) == 0) {
					printf("error reading input\n");
					exit(-1);
				}
				break;
			case '.':
				printf("%i\n", current->value);
				break;
			case ',':
				printf("%c", current->value);
				break;
			case '#':
				if (current->value != 0) {
					current->dir = (current->dir + 1) % 4;
				}
				break;
			default: // sparks die on unknown instructions
				skip = current->prev;
				killSpark(current);
				current = skip;
				continue;
				break;
			}
			current = current->prev;
		}
	}

	fflush(stdout); // should (hopefully) make leaving out a newline not awful
	return 0;
}
